import 'package:c_multi_lines_literals_generator/common/settings.dart';
import 'package:flutter/material.dart';

import 'enums.dart';

class StingModyfierProvider extends ChangeNotifier {
  TextEditingController inputControler = TextEditingController();
  String _userInput = "";
  String output = "Results";
  static const String prefixSlashStyle = "const char* data = \"";
  static const String suffixSlashStyle = "\";";
  static const String prefixConStyle = "std::string data = data + ";
  static const String rawCppStylePrefix = r'std::string data = R"!(';
  static const String rawCppStyleSuffix = r')!";';
  static const String suffixConStyle = ";";
  Settings settings = Settings();

  void getUserInput() {
    _userInput = String.fromCharCodes(inputControler.text.runes);
    inputControler.text = "";
  }

  createMultilineString() {
    output = "";
    _userInput = _processSpecialCharacters(_userInput);
    output += _userInput;
    if (settings.separator == LineSeperatorStyleEnum.slash) {
      slashStyle();
    } else if (settings.separator == LineSeperatorStyleEnum.concatinate) {
      concatenationstyle();
    } else {
      rawCppStringStyle();
    }

    notifyListeners();
  }

  void slashStyle() {
    if (settings.character == EndOfLineStyleEnum.rn) {
      output = output.replaceAll('\n', '\\\\r\n');
    } else {
      output = output.replaceAll('\n', '\\\n');
    }
    output = prefixSlashStyle + output + suffixSlashStyle;
  }

  void concatenationstyle() {
    List<String> linesList = [];
    if (settings.character == EndOfLineStyleEnum.rn) {
      linesList = output.split(r'\r\n');
      linesList = linesList.map((element) {
        return '"' + element + '"+\r\n'; // Modifying each element
      }).toList();
    } else {
      linesList = output.split('\n');
      linesList = linesList.map((element) {
        return '"' + element + '"+\n'; // Modifying each element
      }).toList();
    }

    output = linesList.join();
    output = output.substring(0, output.length - 2);
    output = prefixConStyle + output + suffixConStyle;
  }

  void rawCppStringStyle() {
    if (settings.character == EndOfLineStyleEnum.rn) {
      output = output.replaceAll('\n', '\\\\r\n');
    } else {
      output = output.replaceAll('\n', '\\\n');
    }
    output = rawCppStylePrefix + output + rawCppStyleSuffix;
  }

  String _processSpecialCharacters(String text) {
    text = text.replaceAll(r'\', r'\\');
    text = text.replaceAll(r'"', r'\"');
    text = text.replaceAll('\'', r'\\');
    return text;
  }
}
