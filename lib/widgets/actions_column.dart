import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/enums.dart';
import '../common/stringModyfierProvider.dart';

class ActionsColumn extends StatefulWidget {
  const ActionsColumn({
    super.key,
  });

  @override
  State<ActionsColumn> createState() => _ActionsColumnState();
}

class _ActionsColumnState extends State<ActionsColumn> {
  // EndOfLineStyleEnum? _character = EndOfLineStyleEnum.n;

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<StingModyfierProvider>(context, listen: false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.grey.withOpacity(0.3),
          ),
          child: Column(
            children: [
              ListTile(
                title: const Text('\\ as line separator'),
                leading: Radio<LineSeperatorStyleEnum>(
                  value: LineSeperatorStyleEnum.slash,
                  groupValue: provider.settings.separator,
                  onChanged: (LineSeperatorStyleEnum? value) {
                    setState(() {
                      provider.settings.separator = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: const Text('Concatenation'),
                leading: Radio<LineSeperatorStyleEnum>(
                  value: LineSeperatorStyleEnum.concatinate,
                  groupValue: provider.settings.separator,
                  onChanged: (LineSeperatorStyleEnum? value) {
                    setState(() {
                      provider.settings.separator = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: const Text('Raw string (C++)'),
                leading: Radio<LineSeperatorStyleEnum>(
                  value: LineSeperatorStyleEnum.rawCppStyle,
                  groupValue: provider.settings.separator,
                  onChanged: (LineSeperatorStyleEnum? value) {
                    setState(() {
                      provider.settings.separator = value;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.grey.withOpacity(0.3),
          ),
          child: Column(
            children: [
              ListTile(
                title: const Text('\\n as end of line'),
                leading: Radio<EndOfLineStyleEnum>(
                  value: EndOfLineStyleEnum.n,
                  groupValue: provider.settings.character,
                  onChanged: (EndOfLineStyleEnum? value) {
                    setState(() {
                      provider.settings.character = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: const Text('\\r\\n as end of line'),
                leading: Radio<EndOfLineStyleEnum>(
                  value: EndOfLineStyleEnum.rn,
                  groupValue: provider.settings.character,
                  onChanged: (EndOfLineStyleEnum? value) {
                    setState(() {
                      provider.settings.character = value;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        ElevatedButton(
          onPressed: () {
            var provider =
                Provider.of<StingModyfierProvider>(context, listen: false);
            provider.getUserInput();
            provider.createMultilineString();
          },
          child: const Text("Create string"),
        ),
      ],
    );
  }
}
