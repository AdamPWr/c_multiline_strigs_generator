import 'package:c_multi_lines_literals_generator/widgets/aboutTextWidget.dart';
import 'package:c_multi_lines_literals_generator/widgets/centralWidget.dart';
import 'package:c_multi_lines_literals_generator/widgets/top_bar.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
              color: Colors.black,
              height: size.height,
              child: const Column(
                children: [TopBar(), CentralWidget()],
              )),
          const ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
              child: AboutTextWidget())
        ],
      ),
    );
  }
}
