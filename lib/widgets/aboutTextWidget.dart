import 'package:flutter/material.dart';

class AboutTextWidget extends StatelessWidget {
  const AboutTextWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        padding: const EdgeInsets.all(25),
        color: Colors.grey.withOpacity(0.2),
        child: RichText(
          text: const TextSpan(
            children: [
              TextSpan(
                text: "About the tool\n\n\n",
                style: TextStyle(
                    fontFamily: 'PlayfairDisplaySC',
                    fontSize: 50,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              TextSpan(
                  text:
                      "I've built a nifty online tool that's going to simplify your coding life. "
                      "If you've been wrestling with multilines strings in C, you're in the right place. "
                      "It's a user-friendly C string converter "
                      "that morphs multiline text into C language multiline strings.\n\n"
                      "We all know that C isn't exactly accommodating when it comes to handling strings. "
                      "It's one of those things that makes us wish C would just get with the times. "
                      "The constant breaking down of strings, inserting the correct escape characters, "
                      "and correctly concatenating everything together—it's the stuff of coding nightmares. "
                      "But that's exactly why I created this tool.\n\n"
                      "Simply paste your multiline text into my tool and let it do its thing. "
                      "It'll convert your text into a correctly formatted C multiline string, "
                      "handling all the necessary escape characters and properly concatenating your strings "
                      "so you don't have to. It's all about saving you time and effort, leaving you free "
                      "to tackle the fun stuff.\n\n"
                      "But wait, there's more! The tool isn't just limited to ordinary text. "
                      "It's also perfectly equipped to convert your HTML, CSS, JS files or even SQL queries "
                      "into C-friendly multilines strings. No more hair-pulling as you try to insert your web code "
                      "or database queries into your C programs. This tool is your all-in-one solution.\n\n"
                      "So, whether you're a C rookie getting your feet wet or a seasoned programmer "
                      "who's had more than enough of C's string peculiarities, this C string converter has got your back. "
                      "It's here to take the drudgery out of dealing with multilines strings in C "
                      "and make your coding journey a little more enjoyable.\n\n"
                      "Give it a try, and see how it can transform your coding experience. "
                      "And if you have any thoughts or suggestions, don't hesitate to share. "
                      "I'm always keen to hear from users and make improvements. "
                      "We're all part of this coding adventure together, after all. Happy coding!")
            ],
            style: TextStyle(
                fontFamily: 'PlayfairDisplay',
                fontSize: 30,
                color: Colors.black),
          ),
        ),
      ),
    );
  }
}
