import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/stringModyfierProvider.dart';

class ResultsWidget extends StatelessWidget {
  const ResultsWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
              // color: Colors.green,
              ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Consumer<StingModyfierProvider>(
            builder: (context, provider, child) {
              return SelectableText(
                provider.output,
                minLines: 100,
                maxLines: 150,
              );
            },
          ),
        ),
      ),
    );
  }
}
