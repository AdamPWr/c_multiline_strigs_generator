import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:html' as html;

class TopBar extends StatefulWidget {
  const TopBar({super.key});

  @override
  State<TopBar> createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        height: MediaQuery.of(context).size.height * 0.15,
        width: double.infinity,
        color: Colors.blueAccent,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SvgPicture.asset('assets/c_logo.svg', height: 100),
                const Spacer(),
                const Text(
                  "Multiline c string generator",
                  style: TextStyle(fontSize: 40),
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: InkWell(
                        onTap: () {
                          print("tapped");
                        },
                        child: Material(
                          color: Colors.transparent,
                          child: Ink.image(
                            image: const AssetImage('assets/gitblue.png'),
                            fit: BoxFit.cover,
                            width: 100,
                            height: 100,
                            child: InkWell(
                              onTap: () {
                                html.window.open(
                                    "https://gitlab.com/AdamPWr/c_multiline_strigs_generator",
                                    "");
                              },
                            ),
                          ),
                        ),
                      )),
                ),
              ]),
        ),
      ),
    );
  }
}
// Ink.image(
//                         image: AssetImage('gitblue.png'),
//                         height: 100,
//                         fit: BoxFit.cover,
                      // )