import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/stringModyfierProvider.dart';

class TextColumn extends StatelessWidget {
  const TextColumn({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        minLines: 100,
        maxLines: 150,
        autocorrect: true,
        controller: Provider.of<StingModyfierProvider>(context, listen: false)
            .inputControler,
        decoration: InputDecoration(
          hintText: "Enter your text here",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
      ),
    );
  }
}
