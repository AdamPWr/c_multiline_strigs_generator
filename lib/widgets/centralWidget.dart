import 'package:c_multi_lines_literals_generator/widgets/results_widget.dart';
import 'package:c_multi_lines_literals_generator/widgets/text_column.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/stringModyfierProvider.dart';
import 'actions_column.dart';

class CentralWidget extends StatefulWidget {
  const CentralWidget({super.key});

  @override
  State<CentralWidget> createState() => _CentralWidgetState();
}

class _CentralWidgetState extends State<CentralWidget> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Material(
        child: ChangeNotifierProvider(
          create: (context) => StingModyfierProvider(),
          child: const Row(
            children: [
              Expanded(flex: 4, child: TextColumn()),
              Expanded(flex: 2, child: ActionsColumn()),
              Expanded(flex: 4, child: ResultsWidget())
            ],
          ),
        ),
      ),
    );
  }
}
